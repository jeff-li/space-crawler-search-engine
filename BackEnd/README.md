-- LAB 2 BACK-END ---

CREATING EC2 INSTANCES ON AWS: Run ec2.py

Public IP Address: 54.172.65.200

-- LAB 1 INSTRUCTIONS FOR RUNNING BACK-END CODE ---

Crawler back end is in the file crawler.py. Run the crawler with:

    python crawler.py

Crawler unit tests are in the file crawlerTest.py. Run the crawler unit tests with:

    python crawlerTest.py

