
# Copyright (C) 2011 by Peter Goodman
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

import urllib2
import urlparse
from BeautifulSoup import *
from collections import defaultdict
import re
import boto.dynamodb
import boto.dynamodb2

CREDENTIALS_FILE = '../aws_credentials.conf'

# dynamodb tables
DOC_IDS_TABLE = 'doc_ids'
WORD_IDS_TABLE = 'word_ids'
INVERTED_INDEX_TABLE = 'inverted_index'
PAGE_RANK_TABLE = 'page_rank'
UNIQUE_IDS_TABLE = 'unique_ids'

def attr(elem, attr):
    """An html attribute from an html element. E.g. <a href="">, then
    attr(elem, "href") will get the href or an empty string."""
    try:
        return elem[attr]
    except:
        return ""

WORD_SEPARATORS = re.compile(r'\s|\n|\r|\t|[^a-zA-Z0-9\-_]')

class crawler(object):
    """Represents 'Googlebot'. Populates a database by crawling and indexing
    a subset of the Internet.

    This crawler keeps track of font sizes and makes it simpler to manage word
    ids and document ids."""

    def __init__(self, db_conn, url_file):
        """Initialize the crawler with a connection to the database to populate
        and with the file containing the list of seed URLs to begin indexing."""
        self._url_queue = [ ]
        self._doc_id_cache = { }
        self._word_id_cache = { }
        self._inverted_index_cache = dict()

        self._dynamodb_conn = None
        self._dynamodb_tables = {}
        self._doc_id_table = None
        self._word_id_table = None
        self._links = []

        # functions to call when entering and exiting specific tags
        self._enter = defaultdict(lambda *a, **ka: self._visit_ignore)
        self._exit = defaultdict(lambda *a, **ka: self._visit_ignore)

        # add a link to our graph, and indexing info to the related page
        self._enter['a'] = self._visit_a

        # record the currently indexed document's title an increase
        # the font size
        def visit_title(*args, **kargs):
            self._visit_title(*args, **kargs)
            self._increase_font_factor(7)(*args, **kargs)

        # increase the font size when we enter these tags
        self._enter['b'] = self._increase_font_factor(2)
        self._enter['strong'] = self._increase_font_factor(2)
        self._enter['i'] = self._increase_font_factor(1)
        self._enter['em'] = self._increase_font_factor(1)
        self._enter['h1'] = self._increase_font_factor(7)
        self._enter['h2'] = self._increase_font_factor(6)
        self._enter['h3'] = self._increase_font_factor(5)
        self._enter['h4'] = self._increase_font_factor(4)
        self._enter['h5'] = self._increase_font_factor(3)
        self._enter['title'] = visit_title

        # decrease the font size when we exit these tags
        self._exit['b'] = self._increase_font_factor(-2)
        self._exit['strong'] = self._increase_font_factor(-2)
        self._exit['i'] = self._increase_font_factor(-1)
        self._exit['em'] = self._increase_font_factor(-1)
        self._exit['h1'] = self._increase_font_factor(-7)
        self._exit['h2'] = self._increase_font_factor(-6)
        self._exit['h3'] = self._increase_font_factor(-5)
        self._exit['h4'] = self._increase_font_factor(-4)
        self._exit['h5'] = self._increase_font_factor(-3)
        self._exit['title'] = self._increase_font_factor(-7)

        # never go in and parse these tags
        self._ignored_tags = set([
            'meta', 'script', 'link', 'meta', 'embed', 'iframe', 'frame',
            'noscript', 'object', 'svg', 'canvas', 'applet', 'frameset',
            'textarea', 'style', 'area', 'map', 'base', 'basefont', 'param',
            ])

        # set of words to ignore
        self._ignored_words = set([
            '', 'the', 'of', 'at', 'on', 'in', 'is', 'it',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
            'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
            'u', 'v', 'w', 'x', 'y', 'z', 'and', 'or',
            ])

        # TODO remove me in real version
        self._mock_next_doc_id = 1
        self._mock_next_word_id = 1

        # keep track of some info about the page we are currently parsing
        self._curr_depth = 0
        self._curr_url = ""
        self._curr_doc_id = 0
        self._font_size = 0
        self._curr_words = None

        # get all urls into the queue
        try:
            with open(url_file, 'r') as f:
                for line in f:
                    self._url_queue.append((self._fix_url(line.strip(), ""), 0))
        except IOError:
            pass

        self._init_dynamodb();

    def _get_item(self, table_name, key) :
        item = None;
        try :
            table = self._dynamodb_tables[table_name];
            item = table.get_item(key);
        except boto.dynamodb.exceptions.DynamoDBKeyNotFoundError :
            pass
        return item;

    def _init_dynamodb(self) :
        # Load aws credentials.
        ACCESS_KEY_ID = ''
        SECRET_ACCESS_KEY = ''
        file = open(CREDENTIALS_FILE);
        for line in file :
            line = line.strip()
            # Skip comments and empty lines
            if line.startswith('#') or line == '' :
                continue
            tokens = line.split(':');
            if ACCESS_KEY_ID == '' :
                ACCESS_KEY_ID = tokens[1]
            elif SECRET_ACCESS_KEY == '' :
                SECRET_ACCESS_KEY = tokens[1]
                break;
        # Establish connection.
        self._dynamodb_conn = boto.dynamodb.connect_to_region(
            'us-east-1',
            aws_access_key_id = ACCESS_KEY_ID,
            aws_secret_access_key = SECRET_ACCESS_KEY);
        # Initialize tables.
        dynamodb_tables = self._dynamodb_conn.list_tables();
        table_schema = self._dynamodb_conn.create_schema(
            hash_key_name = 'name',
            hash_key_proto_value = str)
        if DOC_IDS_TABLE not in dynamodb_tables :
            self._dynamodb_conn.create_table(
                name = DOC_IDS_TABLE,
                schema = table_schema,
                read_units = 10,
                write_units = 10);
        if WORD_IDS_TABLE not in dynamodb_tables :
            self._dynamodb_conn.create_table(
                name = WORD_IDS_TABLE,
                schema = table_schema,
                read_units = 10,
                write_units = 10);
        if UNIQUE_IDS_TABLE not in dynamodb_tables :
            self._dynamodb_conn.create_table(
                name = UNIQUE_IDS_TABLE,
                schema = table_schema,
                read_units = 10,
                write_units = 10);
        table_schema = self._dynamodb_conn.create_schema(
            hash_key_name = 'id',
            hash_key_proto_value = int)
        if INVERTED_INDEX_TABLE not in dynamodb_tables :
            self._dynamodb_conn.create_table(
                name = INVERTED_INDEX_TABLE,
                schema = table_schema,
                read_units = 10,
                write_units = 10);
        if PAGE_RANK_TABLE not in dynamodb_tables :
            self._dynamodb_conn.create_table(
                name = PAGE_RANK_TABLE,
                schema = table_schema,
                read_units = 10,
                write_units = 10);
        dynamodb_tables = self._dynamodb_conn.list_tables();
        for table in dynamodb_tables :
            self._dynamodb_tables[table] = self._dynamodb_conn.get_table(table);

        # Load unique ids.
        table = self._dynamodb_tables[UNIQUE_IDS_TABLE];
        item = self._get_item(UNIQUE_IDS_TABLE, 'doc_id');
        if item is None :
            data = {
            'id' : self._mock_next_doc_id
            };
            item = table.new_item(
                hash_key = 'doc_id',
                attrs = data
            );
            item.put();
        else :
            self._mock_next_doc_id = item['id'];
        item = self._get_item(UNIQUE_IDS_TABLE, 'word_id');
        if item is None :
            data = {
                'id' : self._mock_next_word_id
            };
            item = table.new_item(
                hash_key = 'word_id',
                attrs = data
            );
            item.put();
        else :
            self._mock_next_word_id = item['id'];

    # Resolves a document_id to a document string.
    def _document_id_to_document(self, document_id) :
        for row in self._doc_id_table :
            if row['id'] == document_id :
                return row['name'];

    # Resolves a word_id to a word string.
    def _word_id_to_word(self, word_id) :
        for row in self._word_id_table :
            if row['id'] == word_id :
                return row['name'];

    # Resolves the inverted index to strings.
    def get_resolved_inverted_index(self) :
        inverted_index = self.get_inverted_index();
        resolved_inverted_index = dict()
        self._doc_id_table = self._dynamodb_tables[DOC_IDS_TABLE].scan();
        self._word_id_table = self._dynamodb_tables[WORD_IDS_TABLE].scan();
        for word_id in inverted_index :
            word = self._word_id_to_word(word_id)
            resolved_inverted_index[word] = set()
            for document_id in inverted_index[word_id] :
                document = self._document_id_to_document(document_id)
                resolved_inverted_index[word].add(document)
        return resolved_inverted_index

    # Returns the inverted index cache.
    def get_inverted_index(self) :
        self._inverted_index_cache = {};
        table = self._dynamodb_tables[INVERTED_INDEX_TABLE];
        rows = table.scan();
        for row in rows :
            self._inverted_index_cache[row['id']] = row['urls'];
        return self._inverted_index_cache

    def _insert_inverted_index(self, word) :
        word_id = self.word_id(word);
        item = self._get_item(INVERTED_INDEX_TABLE, word_id);
        table = self._dynamodb_tables[INVERTED_INDEX_TABLE];
        if item is None :
            document_id = self.document_id(self._curr_url);
            urls = set();
            urls.add(document_id);
            data = {
            'urls' : urls,
            };
            item = table.new_item(
                hash_key = word_id,
                attrs = data
            );
            item.put();
        else :
            document_id = self.document_id(self._curr_url);
            item['urls'].add(document_id);
            item.put();

    def _page_rank(self, links, num_iterations=20, initial_pr=1.0):
        from collections import defaultdict
        import numpy as np

        page_rank = defaultdict(lambda: float(initial_pr))
        num_outgoing_links = defaultdict(float)
        incoming_link_sets = defaultdict(set)
        incoming_links = defaultdict(lambda: np.array([]))
        damping_factor = 0.85

        # collect the number of outbound links and the set of all incoming documents
        # for every document
        for (from_id,to_id) in links:
            num_outgoing_links[int(from_id)] += 1.0
            incoming_link_sets[to_id].add(int(from_id))

        # convert each set of incoming links into a numpy array
        for doc_id in incoming_link_sets:
            incoming_links[doc_id] = np.array([from_doc_id for from_doc_id in incoming_link_sets[doc_id]])

        num_documents = float(len(num_outgoing_links))
        lead = (1.0 - damping_factor) / num_documents
        partial_PR = np.vectorize(lambda doc_id: page_rank[doc_id] / num_outgoing_links[doc_id])

        for _ in xrange(num_iterations):
            for doc_id in num_outgoing_links:
                tail = 0.0
                if len(incoming_links[doc_id]):
                    tail = damping_factor * partial_PR(incoming_links[doc_id]).sum()
                page_rank[doc_id] = lead + tail

        return page_rank

    def _insert_page_ranks(self) :
        empty = [];
        if self._links == empty :
            return;

        page_ranks = self._page_rank(self._links);
        table = self._dynamodb_tables[PAGE_RANK_TABLE];
        for (k, v) in page_ranks.iteritems() :
            data = {
            'rank' : v,
            }
            item = table.new_item(
                hash_key = k,
                attrs = data
            );
            item.put();

    def _insert_unique_ids(self) :
        # Save unique ids.
        table = self._dynamodb_tables[UNIQUE_IDS_TABLE];
        item = self._get_item(UNIQUE_IDS_TABLE, 'doc_id');
        item['id'] = self._mock_next_doc_id;
        item.put();

        item = self._get_item(UNIQUE_IDS_TABLE, 'word_id');
        item['id'] = self._mock_next_word_id;
        item.put();


    # TODO remove me in real version
    def _mock_insert_document(self, url):
        """A function that pretends to insert a url into a document db table
        and then returns that newly inserted document's id."""
        ret_id = self._mock_next_doc_id
        data = {
        'id' : self._mock_next_doc_id
        }
        table = self._dynamodb_tables[DOC_IDS_TABLE];
        item = table.new_item(
            hash_key = url,
            attrs = data
        );
        item.put();
        self._mock_next_doc_id += 1
        return ret_id

    # TODO remove me in real version
    def _mock_insert_word(self, word):
        """A function that pretends to inster a word into the lexicon db table
        and then returns that newly inserted word's id."""
        ret_id = self._mock_next_word_id
        data = {
        'id' : self._mock_next_word_id
        }
        table = self._dynamodb_tables[WORD_IDS_TABLE];
        item = table.new_item(
            hash_key = word,
            attrs = data
        );
        item.put();
        self._mock_next_word_id += 1
        return ret_id

    def word_id(self, word):
        """Get the word id of some specific word."""
        item = self._get_item(WORD_IDS_TABLE, word);
        if item is not None :
            return item['id'];

        # TODO: 1) add the word to the lexicon, if that fails, then the
        #          word is in the lexicon
        #       2) query the lexicon for the id assigned to this word, 
        #          store it in the word id cache, and return the id.

        word_id = self._mock_insert_word(word)
        self._word_id_cache[word] = word_id
        return word_id

    def document_id(self, url):
        """Get the document id for some url."""
        item = self._get_item(DOC_IDS_TABLE, url);
        if item is not None :
            return item['id'];

        # TODO: just like word id cache, but for documents. if the document
        #       doesn't exist in the db then only insert the url and leave
        #       the rest to their defaults.

        doc_id = self._mock_insert_document(url)
        self._doc_id_cache[url] = doc_id
        return doc_id

    def _fix_url(self, curr_url, rel):
        """Given a url and either something relative to that url or another url,
        get a properly parsed url."""

        rel_l = rel.lower()
        if rel_l.startswith("http://") or rel_l.startswith("https://"):
            curr_url, rel = rel, ""

        # compute the new url based on import 
        curr_url = urlparse.urldefrag(curr_url)[0]
        parsed_url = urlparse.urlparse(curr_url)
        return urlparse.urljoin(parsed_url.geturl(), rel)

    def add_link(self, from_doc_id, to_doc_id):
        """Add a link into the database, or increase the number of links between
        two pages in the database."""
        link_list_element = (from_doc_id, to_doc_id);
        self._links.append(link_list_element);

    def _visit_title(self, elem):
        """Called when visiting the <title> tag."""
        title_text = self._text_of(elem).strip()
        print "document title="+ repr(title_text)

        # TODO update document title for document id self._curr_doc_id

    def _visit_a(self, elem):
        """Called when visiting <a> tags."""

        dest_url = self._fix_url(self._curr_url, attr(elem,"href"))

        #print "href="+repr(dest_url), \
        #      "title="+repr(attr(elem,"title")), \
        #      "alt="+repr(attr(elem,"alt")), \
        #      "text="+repr(self._text_of(elem))

        # add the just found URL to the url queue
        self._url_queue.append((dest_url, self._curr_depth))

        # add a link entry into the database from the current document to the
        # other document
        self.add_link(self._curr_doc_id, self.document_id(dest_url))

        # TODO add title/alt/text to index for destination url

    def _add_words_to_document(self):
        # TODO: knowing self._curr_doc_id and the list of all words and their
        #       font sizes (in self._curr_words), add all the words into the
        #       database for this document
        print "    num words="+ str(len(self._curr_words))

    def _increase_font_factor(self, factor):
        """Increade/decrease the current font size."""
        def increase_it(elem):
            self._font_size += factor
        return increase_it

    def _visit_ignore(self, elem):
        """Ignore visiting this type of tag"""
        pass

    def _add_text(self, elem):
        """Add some text to the document. This records word ids and word font sizes
        into the self._curr_words list for later processing."""
        words = WORD_SEPARATORS.split(elem.string.lower())
        for word in words:
            word = word.strip()
            if word in self._ignored_words:
                continue
            self._curr_words.append((self.word_id(word), self._font_size))
            self._insert_inverted_index(word)

    def _text_of(self, elem):
        """Get the text inside some element without any tags."""
        if isinstance(elem, Tag):
            text = [ ]
            for sub_elem in elem:
                text.append(self._text_of(sub_elem))

            return " ".join(text)
        else:
            return elem.string

    def _index_document(self, soup):
        """Traverse the document in depth-first order and call functions when entering
        and leaving tags. When we come accross some text, add it into the index. This
        handles ignoring tags that we have no business looking at."""
        class DummyTag(object):
            next = False
            name = ''

        class NextTag(object):
            def __init__(self, obj):
                self.next = obj

        tag = soup.html
        stack = [DummyTag(), soup.html]

        while tag and tag.next:
            tag = tag.next

            # html tag
            if isinstance(tag, Tag):

                if tag.parent != stack[-1]:
                    self._exit[stack[-1].name.lower()](stack[-1])
                    stack.pop()

                tag_name = tag.name.lower()

                # ignore this tag and everything in it
                if tag_name in self._ignored_tags:
                    if tag.nextSibling:
                        tag = NextTag(tag.nextSibling)
                    else:
                        self._exit[stack[-1].name.lower()](stack[-1])
                        stack.pop()
                        tag = NextTag(tag.parent.nextSibling)

                    continue

                # enter the tag
                self._enter[tag_name](tag)
                stack.append(tag)

            # text (text, cdata, comments, etc.)
            else:
                self._add_text(tag)

    def crawl(self, depth=2, timeout=3):
        """Crawl the web!"""
        seen = set()

        while len(self._url_queue):

            url, depth_ = self._url_queue.pop()

            # skip this url; it's too deep
            if depth_ > depth:
                continue

            doc_id = self.document_id(url)

            # we've already seen this document
            if doc_id in seen:
                continue

            seen.add(doc_id) # mark this document as haven't been visited

            socket = None
            try:
                socket = urllib2.urlopen(url, timeout=timeout)
                soup = BeautifulSoup(socket.read())

                self._curr_depth = depth_ + 1
                self._curr_url = url
                self._curr_doc_id = doc_id
                self._font_size = 0
                self._curr_words = [ ]
                self._index_document(soup)
                self._add_words_to_document()
                print "    url="+repr(self._curr_url)
            except Exception as e:
                print e
                pass
            finally:
                if socket:
                    socket.close()
        self._insert_page_ranks();
        self._insert_unique_ids();

if __name__ == "__main__":
    bot = crawler(None, "urls.txt")
    bot.crawl(depth=1)
    #print bot.get_inverted_index()
    #print bot.get_resolved_inverted_index()
