import unittest
from crawler import crawler


class crawlerUnitTests(unittest.TestCase) :

    def setUp(self) :
        self.bot = crawler(None, "urls.txt")

    # Test resolving a word_id (integer) to a word (string).
    def test_word_id_to_word(self) :
        test_word_id = 1
        test_word = "word"
        self.bot._word_id_cache[test_word] = test_word_id
        # Should be able to resolve the word string from the word_id
        # that was just inserted.
        self.assertEqual(self.bot._word_id_to_word(test_word_id), test_word)

    # Test resolving a non existent word id.
    def test_word_id_to_word_nonexistent_word_id(self) :
        nonexistent_word_id = 1
        self.assertFalse(self.bot._word_id_to_word(nonexistent_word_id))

    # Test resolving a document_id (integer) to a document (string).
    def test_document_id_to_document(self) :
        test_document_id = 1
        test_document = "document"
        self.bot._doc_id_cache[test_document] = test_document_id
        # Should be able to resolve the document string from the document_id
        # that was just inserted.
        self.assertEqual(self.bot._document_id_to_document(test_document_id), test_document)
    # Test resolving a nonexistent document id.
    def test_document_id_to_document_nonexistent_document_id(self) :
        nonexistent_document_id = 1
        self.assertFalse(self.bot._document_id_to_document(nonexistent_document_id))

    # Test retreiving an inverted index.
    def test_get_inverted_index(self) :
        test_inverted_index = dict()
        test_index = 1
        test_document_ids = { 1, 2 }
        test_inverted_index[test_index] = test_document_ids
        self.bot._inverted_index_cache = test_inverted_index
        # Should retreive the inverted index just inserted.
        self.assertEqual(self.bot.get_inverted_index(), test_inverted_index)

    # Test getting an empty inverted index.
    def test_get_inverted_index_empty(self) :
        expected_inverted_index = {}
        self.assertEqual(self.bot.get_inverted_index(), expected_inverted_index)

    # This test trys creating an inverted index for a word.
    # The test case tries to map one word to two different URLs.
    def test_insert_inverted_index(self) :
        # Setup document and word caches for test.
        test_word = "word"
        test_word_id = "1"
        test_docs = { "http://url1.com", "http://url2.com" }
        test_doc_ids = set()
        self.bot._word_id_cache[test_word] = test_word_id
        for doc in test_docs :
             test_doc_ids.add(self.bot.document_id(doc))
        
        # Test inserting inverted index entries.
        for doc in test_docs :
             self.bot._curr_url = doc
             self.bot._insert_inverted_index(test_word)

        # Make sure inverted index entries were put in correctly.
        expected_inverted_index = dict()
        expected_inverted_index[test_word_id] = test_doc_ids
        self.assertEquals(self.bot.get_inverted_index(), expected_inverted_index)

    # Test if resolving inverted index works.
    def test_get_resolved_inverted_index(self) :
        # Create an inverted index entry for one word and one URL.
        test_word = "word"
        test_url = "http://www.url.com"
        self.bot.word_id(test_word)
        self.bot.document_id(test_url)
        self.bot._curr_url = test_url
        self.bot._insert_inverted_index(test_word)

        # See if entrie for word/URL pair is resolved correctly.
        expected_resolved_inverted_index = dict()
        expected_resolved_inverted_index[test_word] = set()
        expected_resolved_inverted_index[test_word].add(test_url)
        self.assertEquals(self.bot.get_resolved_inverted_index(), expected_resolved_inverted_index)

if __name__ == '__main__':
    unittest.main()

