#!/bin/bash

#    ____                          ____                    _
#   / ___| _ __   __ _  ___ ___ _ / ___|_ __ __ ___      _| | ___ _ __ 
#   \___ \| '_ \ / _` |/ __/ _ (*) |   | '__/ _` \ \ /\ / / |/ _ \ '__|
#    ___) | |_) | (_| | (_|  __/_| |___| | | (_| |\ V  V /| |  __/ |   
#   |____/| .__/ \__,_|\___\___(*)\____|_|  \__,_| \_/\_/ |_|\___|_|   
#         |_|                                                          
#
# Installation Setup Script

echo " _______  _______  _______  _______  _______  ___   _______  ______    _______  _     _  ___      _______  ______   ";
echo "|       ||       ||   _   ||       ||       ||   | |       ||    _ |  |   _   || | _ | ||   |    |       ||    _ |  ";
echo "|  _____||    _  ||  |_|  ||       ||    ___||___| |       ||   | ||  |  |_|  || || || ||   |    |    ___||   | ||  ";
echo "| |_____ |   |_| ||       ||       ||   |___  ___  |       ||   |_||_ |       ||       ||   |    |   |___ |   |_||_ ";
echo "|_____  ||    ___||       ||      _||    ___||   | |      _||    __  ||       ||       ||   |___ |    ___||    __  |";
echo " _____| ||   |    |   _   ||     |_ |   |___ |___| |     |_ |   |  | ||   _   ||   _   ||       ||   |___ |   |  | |";
echo "|_______||___|    |__| |__||_______||_______|      |_______||___|  |_||__| |__||__| |__||_______||_______||___|  |_|";


echo "";
echo -e "==============================================================\n"
echo -e "==                  Installing Space:Crawler                ==\n"
echo -e "==============================================================\n"
echo "";
echo "";
echo ">>>>>> Downloading and installing pip package management system...";
echo "------ Downloading get-pip.py";
wget https://bootstrap.pypa.io/get-pip.py
echo "------ Installing get-pip.py";
sudo python get-pip.py
echo "------ DONE";
echo "";
echo "";
echo ">>>>>> Installing google-api-python-client...";
sudo pip install google-api-python-client
echo ">>>>>> Done";
echo "";
echo "";
echo ">>>>>> Installing oauth2client...";
sudo pip install oauth2client
echo ">>>>>> Done";
echo "";
echo "";
echo ">>>>>>Installing beaker...";
sudo pip install beaker
echo ">>>>>> Done";
echo "";
echo "";
echo ">>>>>> Installing BOTO...";
sudo pip install boto;
echo ">>>>>> Done";
echo "";
echo "";
echo ">>>>>> If there are any errors above, please try to reinstall the module again using pip"
echo "";
echo "";

echo -e "==============================================================\n"
echo -e "==                  Launching Space:Crawler                 ==\n"
echo -e "==============================================================\n"
echo "";

echo -ne '[                              ] (10%)\r'
sleep 1
echo -ne '[===                           ] (10%)\r'
sleep 1
echo -ne '[======                        ] (20%)\r'
sleep 1
echo -ne '[=========                     ] (30%)\r'
sleep 1
echo -ne '[============                  ] (40%)\r'
sleep 1
echo -ne '[===============               ] (50%)\r'
sleep 1
echo -ne '[==================            ] (60%)\r'
sleep 1
echo -ne '[=====================         ] (70%)\r'
sleep 1
echo -ne '[========================      ] (80%)\r'
sleep 1
echo -ne '[===========================   ] (90%)\r'
sleep 1
echo -ne '[==============================] (100%)\r'
echo -ne '\n'

cd ..
cd FrontEnd
sudo nohup python app.py >& ../server.log &
echo "Successfully launched Space:Crawler!";
echo "    server logs can be found in server.log";



