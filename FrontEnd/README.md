-- INSTRUCTIONS FOR RUNNING FRONT-END CODE ---

The front end application is in the file app.py. The server can be started by using the command:

    python app.py
  

When the server is running, you can access the webpage in the browser with address:
    http://localhost:8080
