from bottle import route, run, template, static_file, request, redirect, app, TEMPLATES, response
from oauth2client.client import OAuth2WebServerFlow
from oauth2client.client import flow_from_clientsecrets
from googleapiclient.discovery import build
from oauth2client.file import Storage
from beaker.middleware import SessionMiddleware

import httplib2
import config
import os

import boto.dynamodb
import boto.dynamodb2
import boto.dynamodb2.table
import json


print ""
print ""
print " _______  _______  _______  _______  _______  ___   _______  ______    _______  _     _  ___      _______  ______   "
print "|       ||       ||   _   ||       ||       ||   | |       ||    _ |  |   _   || | _ | ||   |    |       ||    _ |  "
print "|  _____||    _  ||  |_|  ||       ||    ___||___| |       ||   | ||  |  |_|  || || || ||   |    |    ___||   | ||  "
print "| |_____ |   |_| ||       ||       ||   |___  ___  |       ||   |_||_ |       ||       ||   |    |   |___ |   |_||_ "
print "|_____  ||    ___||       ||      _||    ___||   | |      _||    __  ||       ||       ||   |___ |    ___||    __  |"
print " _____| ||   |    |   _   ||     |_ |   |___ |___| |     |_ |   |  | ||   _   ||   _   ||       ||   |___ |   |  | |"
print "|_______||___|    |__| |__||_______||_______|      |_______||___|  |_||__| |__||__| |__||_______||_______||___|  |_|"
print ""
print "By:  GROUP NUMBER: 9"
print "    Jeff Weizhong Li  998083112   UTORid:liweizho"
print "    Ahsan Sardar      998422158   UTORid:sardarmo"
print ""

ACCESS_KEY_ID = 'AKIAI24A3OKN3YPPFLEA'
SECRET_ACCESS_KEY = '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'

# dynamodb table names
DOC_IDS_TABLE = 'doc_ids'
WORD_IDS_TABLE = 'word_ids'
INVERTED_INDEX_TABLE = 'inverted_index'
PAGE_RANK_TABLE = 'page_rank'
UNIQUE_IDS_TABLE = 'unique_ids'

# data tables
db_initialized = False
dynamodb_tables = {}
dynamodb_tables_cache = {}


# ******************* Global Variables - store search history and user profile *******************
# app_searched_words = []
# all_user_frequent_words = {}
GOOGLE_LOGGIN_ENABLED = False
all_user_search_history = {}
current_user_info = {}
pagination_result = []


# ******************************** Setups For Beaker User Sessions *******************************

session_opts = {
    'session.type': config.SESSION_TYPE,
    'session.cookie_expires': config.SESSION_COOKIE_EXPIRES,
    'session.data_dir': config.SESSION_DATA_DIR,
    'session.auto': config.SESSION_AUTO
}

app = SessionMiddleware(app(), session_opts)


# **************************************  Helper Functions   *************************************


def update_dict(d, word):
    """
    This function will update the record of a non-whitespace word in the given dictionary.
    If the word already exists in the dictionary, its value will be incremented by 1, other wise
    a new entry will be created.

        Args:
        d (dictionary): a dictionary that needs to be updated
        word (str): a word that needs to be added to the search history
    """
    if word != '':  # if the word was not a whitespace
        if word in d:
            d[word] += 1
        else:
            d[word] = 1


def update_unique_list(l, word):
    """
    This function adds a word to a list l if word doesnt not exit in l
    """
    if word != '':  # if the word was not a whitespace
        if word not in l:
            l.append(word)
        else:
            return


def get_recent_ten():
    """
    This function return the 10 most recent searches of a logged in user
    """
    session = request.environ.get('beaker.session')

    if 'logged_in' in session:
        return all_user_search_history[session['usr_id']]
    else:
        return []


def parse_keywords(keywords):
    """
    This function will parse the query string and update the search history.
    It takes the query string as a parameter and then split the string into individual words.
    The function will then sanitize each word and count the number of appearance for each word.

        Args:
        keywords (str): the query string submitted by user

    """

    session = request.environ.get('beaker.session')
    temp = keywords.split(" ")
    result = {}

    if 'logged_in' in session:
        temp_hist = all_user_search_history[session['usr_id']]

        for word in temp:
            sanitized_word = word.translate(None, '!,.?')  # remove punctuations
            update_dict(result, sanitized_word)  # create search result
            temp_hist.insert(0, sanitized_word)

        if len(temp_hist) > 10:
            temp_hist = temp_hist[0:10]
        all_user_search_history[session['usr_id']] = temp_hist

    else:
        for word in temp:
            sanitized_word = word.translate(None, '!,.?')  # remove punctuations
            update_dict(result, sanitized_word)  # create search result
    return result


def init_and_cache_dbs():
    global dynamodb_tables_cache
    w_id = {}
    inverted_idx = {}
    pg_rank = {}
    doc_id = {}
    word_db = []

    print "========> initializing database cache"
    try:
        dynamodb_conn = boto.dynamodb.connect_to_region(
            'us-east-1',
            aws_access_key_id=ACCESS_KEY_ID,
            aws_secret_access_key=SECRET_ACCESS_KEY)
    except Exception:
        redirect('/error')

    try:
        dynamodb_table_names = dynamodb_conn.list_tables()
    except Exception:
        redirect('/error')

    for db_name in dynamodb_table_names:
        dynamodb_tables[str(db_name)] = dynamodb_conn.get_table(str(db_name))

    try:
        word_id_rows = dynamodb_tables[WORD_IDS_TABLE].scan()
    except Exception:
        redirect('/error')
    for row in word_id_rows:
        temp = {}
        w_id[row['name']] = row['id']
        temp['name'] = row['name']
        word_db.append(temp)
    dynamodb_tables_cache[WORD_IDS_TABLE] = w_id

    file_name = 'words_data.json'
    script_dir = os.path.dirname(os.path.realpath(__file__))
    dest_dir = os.path.join(script_dir, 'data')
    try:
        os.makedirs(dest_dir)
    except OSError:
        pass  # already exists
    path = os.path.join(dest_dir, file_name)

    with open(path, 'wb') as fp:
        json.dump(word_db, fp)

    try:
        inverted_index_rows = dynamodb_tables[INVERTED_INDEX_TABLE].scan()
    except Exception:
        redirect('/error')
    for row in inverted_index_rows:
        inverted_idx[row['id']] = row['urls']
    dynamodb_tables_cache[INVERTED_INDEX_TABLE] = inverted_idx

    try:
        page_rank_rows = dynamodb_tables[PAGE_RANK_TABLE].scan()
    except Exception:
        redirect('/error')
    for row in page_rank_rows:
        pg_rank[row['id']] = row['rank']
    dynamodb_tables_cache[PAGE_RANK_TABLE] = pg_rank

    try:
        doc_id_rows = dynamodb_tables[DOC_IDS_TABLE].scan()
    except Exception:
        redirect('/error')
    for row in doc_id_rows:
        doc_id[row['name']] = row['id']
    dynamodb_tables_cache[DOC_IDS_TABLE] = doc_id


def search_from_db(keywords):
    global dynamodb_tables
    global db_initialized
    result = []
    doc_id_with_rank = []
    word = keywords.split(" ")[0]

    if not db_initialized:
        init_and_cache_dbs()
        db_initialized = True


    # print dynamodb_tables
    # print word

    if word not in dynamodb_tables_cache[WORD_IDS_TABLE]:
        # print "word not in the dict"
        return None
    else:
        word_id = dynamodb_tables_cache[WORD_IDS_TABLE][word]
        # print "found word"
        # print word_id

    inv_idx = dynamodb_tables_cache[INVERTED_INDEX_TABLE][word_id]
    # print inv_idx

    for doc_id in inv_idx:
        doc_id_with_rank.append((dynamodb_tables_cache[PAGE_RANK_TABLE][doc_id], doc_id))

    doc_id_with_rank.sort(reverse=True)

    for pair in doc_id_with_rank:
        result += [str(k) for k, v in dynamodb_tables_cache[DOC_IDS_TABLE].items() if v == pair[1]]

    # print result
    return result


# **************************************  Bottle web framework  *************************************

@route('/js/<filename>')
def js_static(filename):
    """ serve static JavaScript files """
    return static_file(filename, root='./js')


@route('/css/<filename>')
def css_static(filename):
    """ serve static CSS files """
    return static_file(filename, root='./css')


@route('/data/<filename>')
def data_static(filename):
    """ serve static JSON files """
    return static_file(filename, root='./data')


@route('/img/<filename>')
def img_static(filename):
    """ serve static image files """
    return static_file(filename, root='./img')


@route('/<filename>')
def json_static(filename):
    """ serve static JSON files """
    return static_file(filename, root='json')


@route('/init', method='GET')  # use HTTP GET method
def init():
    return ["result"]


@route('/error')  # use HTTP GET method
def db_error():
    """ error handling page for database errors"""
    session = request.environ.get('beaker.session')
    user_profile = {}
    validated = False

    if 'logged_in' in session:
        validated = True
        user_profile = session['user_profile']

    return template('error',
                    profile=user_profile,
                    title=config.SITE_TITLE,
                    validated=validated)


@route('/404')  # use HTTP GET method
def not_found():
    """ error handling page if a word is not found in the database """
    session = request.environ.get('beaker.session')
    user_profile = {}
    validated = False

    if 'logged_in' in session:
        validated = True
        user_profile = session['user_profile']

    return template('404',
                    profile=user_profile,
                    title=config.SITE_TITLE,
                    validated=validated)


@route('/notification')  # use HTTP GET method
def app_notification():
    """ Display any application notifications """
    session = request.environ.get('beaker.session')
    user_profile = {}
    validated = False

    if 'logged_in' in session:
        validated = True
        user_profile = session['user_profile']

    return template('notification',
                    profile=user_profile,
                    title=config.SITE_TITLE,
                    validated=validated)


@route('/redirect', method='GET')
def redirect_page():
    session = request.environ.get('beaker.session')

    if GOOGLE_LOGGIN_ENABLED:                                   # disable google oAuth Login
        if request.query.get('error', '') is "access_denied":
            redirect('/')

        code = request.query.get('code', '')

        flow = OAuth2WebServerFlow(
            client_id=config.GOOGLE_CLIENT_ID,
            client_secret=config.GOOGLE_CLIENT_SECRET,
            scope=config.GOOGLE_API_SCOPE,
            redirect_uri=config.REDIRECT_URI
        )

        credentials = flow.step2_exchange(code)

        token = credentials.id_token['sub']

        http = httplib2.Http()
        http = credentials.authorize(http)

        storage = Storage('oAuth.data')
        storage.put(credentials)

        users_service = build('oauth2', 'v2', http=http)
        user_document = users_service.userinfo().get().execute()

        user_id = user_document['id']


    user_id = 'test_url'
    user_document = {'name': 'Test User', 'family_name': 'User', 'given_name': 'Test', 'email': 'test.user@gmail.com'}
    user_profile = {}

    if user_id not in all_user_search_history:
        all_user_search_history[user_id] = []

    user_profile['name'] = user_document.get('name', None)
    user_profile['family_name'] = user_document.get('family_name', None)
    user_profile['given_name'] = user_document.get('given_name', None)
    user_profile['picture'] = user_document.get('picture', config.DEFAULT_PHOTO)
    user_profile['gender'] = user_document.get('gender', None)
    user_profile['email'] = user_document.get('email', None)
    user_profile['link'] = user_document.get('link', None)

    session['logged_in'] = True
    session['usr_id'] = user_id
    session['user_profile'] = user_profile
    # session['user_credential'] = credentials

    session.save()
    redirect('/notification')


@route('/logout', method='GET')  # use HTTP GET method
def logout():
    session = request.environ.get('beaker.session')

    if 'logged_in' in session:
        del session['logged_in']

    TEMPLATES.clear()
    redirect('/')


@route('/login', method='GET')  # use HTTP GET method
def search():
    session = request.environ.get('beaker.session')

    if 'logged_in' in session:
        redirect('/')

    else:
        if not GOOGLE_LOGGIN_ENABLED:
            redirect('/redirect')
        else:
            flow = flow_from_clientsecrets(
                config.CLIENT_SECRETS,
                scope=config.GOOGLE_API_SCOPE,
                redirect_uri=config.REDIRECT_URI,
                message=config.MISSING_CLIENT_SECRETS_MESSAGE
            )

            uri = flow.step1_get_authorize_url()
            redirect(str(uri))



@route('/', method='GET')  # use HTTP GET method
def search():
    """ callback function Search """

    session = request.environ.get('beaker.session')

    keywords = request.GET.get('keywords')
    response.set_header("Cache-Control", "no-cache, no-store, must-revalidate")
    response.set_header("Pragma", "no-cache")
    response.set_header("Expires", "0")

    user_profile = {}
    validated = False

    global db_initialized

    if not db_initialized:
        #init_and_cache_dbs()
        db_initialized = True

    if 'logged_in' in session:
        validated = True
        user_profile = session['user_profile']

    if keywords is None or keywords.strip() == '' or keywords == '':  # if the query string is empty
        recent_ten = get_recent_ten()
        return template('home',
                        title=config.SITE_TITLE,
                        validated=validated,
                        recent_ten=recent_ten,
                        profile=user_profile)  # then load the default homepage

    keywords = keywords.strip()  # remove the leading and trailing white space

    keywords_lower = keywords.lower()  # convert everything to lowercase
    result = parse_keywords(keywords_lower)  # parse the query string and create search result
    # search_ranking = get_search_history()           # get top searched words
    recent_ten = get_recent_ten()
    search_result = search_from_db(keywords_lower)

    if search_result is None:
        redirect('/404')
    else:
        return template('results',
                        search_result=search_result,
                        keywords=keywords,
                        result=result,
                        recent_ten=recent_ten,
                        title=config.SITE_TITLE,
                        validated=validated,
                        profile=user_profile)


run(app=app, host='localhost', port=8080, debug=True)

