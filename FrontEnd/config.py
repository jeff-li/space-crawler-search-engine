
SITE_TITLE = "S:C"

CLIENT_SECRETS = "client_secrets.json"
GOOGLE_API_SCOPE = "https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/userinfo.email"
REDIRECT_URI = "http://ec2-54-172-36-144.compute-1.amazonaws.com/redirect"

GOOGLE_CLIENT_ID = ""
GOOGLE_CLIENT_SECRET = ""

# Message to display in the browser if the CLIENT_SECRETS file is missing.
MISSING_CLIENT_SECRETS_MESSAGE = """
WARNING: Please configure OAuth 2.0

To make this sample run you will need to populate the client_secrets.json file.

"""

SESSION_TYPE = "memory"
SESSION_DATA_DIR = './session'
SESSION_AUTO = True
SESSION_COOKIE_EXPIRES = False

DEFAULT_PHOTO = "img/default-photo.jpg"
