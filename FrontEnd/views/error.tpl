<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ title }}</title>
    <meta name="Description" content="Result Page">
    <meta name="Jeff Weizhong Li (998083112)" content="">

    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />

    <!-- styles -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
</head>

<body>
    <div id="loading">
        <img id="loading-icon" src="http://bit.ly/pMtW1K">
    </div>

    %if validated == False:
    <div class="login-panel" id="login-panel">
        <form id="loginForm" action="/login" method="GET">
            <button class="login-button" type="submit">
                <img class="login-img" id="person" />
                <span class="login-text"> SIGN IN </span>
            </button>
        </form>
    </div>

    %else:
    <div class="logout-panel">
          <form id="logoutForm" action="/logout" method="GET" onsubmit="">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-1 profile-sm-frame">
                        <img class="profile-img" src="{{profile['picture']}}" id="person" />
                    </div>
                    <div class="col-sm-7 user-info-area"
                        <span> {{profile['name']}} </span> <br>
                        <span> {{profile['email']}} </span>
                        <button id="logout-button" class="logout-button" type="submit">
                            <span class="logout-text"> LOG OUT </span>
                        </button>
                    </div>
                </div>
           </form>
    </div>
    %end

    <div class="logo-box title-box">
        <h1>  Space  <span class="text-top red-text"> : </span>  Crawler</h1>
        <h3>an <span class="rainbow-color"> intelligent </span> web crawler</h3>
        <a href="/"></a>
    </div>
    <div class="overlay">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 error-msg">
                <h1> Oops! </h1>
                <h4> seems like there's something wrong with our database</h4>
                <h4> please try again later... </h4>
                <h4> <span class="code">  sorry for the inconvenience </span> </h4>

                <img src="../img/404.jpg">

            </div>
        </div>
    </div>



<!-- Load javascript files -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="/js/jquery-1.11.1.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/rainbow.js"></script>


<script type="text/javascript">
    $(function() {
        $(".title-box").click(function(){
            window.location = $(this).find("a:first").attr("href");
            return false;
        });

        $( "#logoutForm" ).submit(function( event ) {
            console.log('here');
            var backlen = history.length;
            history.go(-backlen); // Return at the beginning
            window.location.replace("http://localhost:8080");
        });

        $( "#loginForm" ).submit(function( event ) {
            var form = this;
            $("#loading").css("display", "block");
            event.preventDefault();

            setTimeout( function () {
                form.submit();
            }, 300);
        });

        $("#login-panel").hover(function(){
            $(this).filter(':not(:animated)').animate({ width: "175px" });
        }, function() {
            $(this).animate({ width: "104px" });
        });

        $('.rainbow-color').rainbow({
            colors: [
                '#FF0000',
                '#f26522',
                '#fff200',
                '#00a651',
                //'#28abe2',
                //'#2e3192',
                '#6868ff'
            ],
            animate: true,
            animateInterval: 300,
            pad: false,
            pauseLength: 200
        });
    });
</script>

</body>
</html>