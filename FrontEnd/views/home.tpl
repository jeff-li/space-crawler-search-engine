<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ title }}</title>
    <meta name="Description" content="Home Page">
    <meta name="Jeff Weizhong Li (998083112)" content="">

    <meta http-equiv="Cache-Control" content="no-cache" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />

    <!-- styles -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div id="loading">
        <img id="loading-icon" src="http://bit.ly/pMtW1K">
    </div>

    %if validated == False:
    <div class="login-panel" id="login-panel">
        <form id="loginForm" action="/login" method="GET">
            <button class="login-button" type="submit">
                <img class="login-img" id="person" />
                <span class="login-text"> SIGN IN </span>
            </button>
        </form>
    </div>

    %else:
    <div class="logout-panel">
          <form id="logoutForm" action="/logout" method="GET" onsubmit="">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-1 profile-sm-frame">
                        <img class="profile-img" src="{{profile['picture']}}" id="person" />
                    </div>
                    <div class="col-sm-7 user-info-area"
                        <span> {{profile['name']}} </span> <br>
                        <span> {{profile['email']}} </span>
                        <button id="logout-button" class="logout-button" type="submit">
                            <span class="logout-text"> LOG OUT </span>
                        </button>
                    </div>
                </div>
           </form>
    </div>
    %end

    <div class="title-box">
        <h1>  Space  <span class="text-top red-text"> : </span>  Crawler</h1>
        <h3>an <span class="rainbow-color"> intelligent </span> web crawler</h3>
        <a href="/"></a>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2 content-box">
            <div class="search-box">
                <form id="searchForm" action="/" method="GET">
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="input-group" id="words-prefetch">
                                <input id="keywords" name="keywords" type="text" class="form-control search-input typeahead" value="" autofocus="">
                                <span class="input-group-btn">
                                     <button class="btn btn-default search-btn" type="submit"> GO </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    %if validated == True:
    <div class="row">
        <div class="col-md-3 col-md-offset-3 content-box past-words-box">
            <div class="history-box">
                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="false">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                  View Top 10 Searched Words
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <table id="top_ten" name="top_ten" class="table">
                                    <tr class="bold-title">
                                        <td> word </td>
                                    </tr>
                                    %for word in recent_ten:
                                        <tr>
                                            <td>{{word}}</td>
                                        </tr>
                                    %end
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    %end

<!-- Load javascript files -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="/js/jquery-1.11.1.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/rainbow.js"></script>
<script src="/js/typeahead.bundle.min.js"></script>

<script type="text/javascript">
    $(function() {
        $(".title-box").click(function(){
            window.location = $(this).find("a:first").attr("href");
            return false;
        });
        $( "#logoutForm" ).submit(function( event ) {
            var form = this;
            $("#loading").css("display", "block");
            event.preventDefault();

            setTimeout( function () {
                form.submit();
            }, 300);
        });

        $( "#loginForm" ).submit(function( event ) {
            var form = this;
            $("#loading").css("display", "block");
            event.preventDefault();

            setTimeout( function () {
                form.submit();
            }, 300);
        });

        $("#login-panel").hover(function(){
            $(this).filter(':not(:animated)').animate({ width: "175px" });
        }, function() {
            $(this).animate({ width: "104px" });
        });

        $('.rainbow-color').rainbow({
            colors: [
                '#FF0000',
                '#f26522',
                '#fff200',
                '#00a651',
                //'#28abe2',
                //'#2e3192',
                '#6868ff'
            ],
            animate: true,
            animateInterval: 300,
            pad: false,
            pauseLength: 200
        });

        var words = new Bloodhound({
          datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          limit: 10,
          prefetch: {
            url: '../data/words_data.json'
          }
        });

        words.initialize();

        $('#words-prefetch .typeahead').typeahead({
            highlight: true
        }, {
          name: 'words',
          displayKey: 'name',
          source: words.ttAdapter()
        });
    });
</script>


</body>
</html>