<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ title }}</title>
    <meta name="Description" content="Result Page">
    <meta name="Jeff Weizhong Li (998083112)" content="">

    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />

    <!-- styles -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
</head>

<body>
    <div id="loading">
        <img id="loading-icon" src="http://bit.ly/pMtW1K">
    </div>

    <div class="logo-box title-box">
        <h1>  Space  <span class="text-top red-text"> : </span>  Crawler</h1>
        <h3>an <span class="rainbow-color"> intelligent </span> web crawler</h3>
        <a href="/"></a>
    </div>
    <div class="overlay">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 error-msg">
                <h1> Notification </h1>
                <h4> The Google OAuth2 login feature has been disabled because this </h4>
                <h4> application is generated using dynamic IP address.</h4>
                <h4> You will be logged in as Test User </span> </h4>
                <h4> Go to home page by clicking <a href="/"> here </a></h4>

            </div>
        </div>
    </div>



<!-- Load javascript files -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="/js/jquery-1.11.1.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/rainbow.js"></script>


<script type="text/javascript">
    $(function() {
        $(".title-box").click(function(){
            window.location = $(this).find("a:first").attr("href");
            return false;
        });

        $( "#logoutForm" ).submit(function( event ) {
            console.log('here');
            var backlen = history.length;
            history.go(-backlen); // Return at the beginning
            window.location.replace("http://localhost:8080");
        });

        $( "#loginForm" ).submit(function( event ) {
            var form = this;
            $("#loading").css("display", "block");
            event.preventDefault();

            setTimeout( function () {
                form.submit();
            }, 300);
        });

        $("#login-panel").hover(function(){
            $(this).filter(':not(:animated)').animate({ width: "175px" });
        }, function() {
            $(this).animate({ width: "104px" });
        });

        $('.rainbow-color').rainbow({
            colors: [
                '#FF0000',
                '#f26522',
                '#fff200',
                '#00a651',
                //'#28abe2',
                //'#2e3192',
                '#6868ff'
            ],
            animate: true,
            animateInterval: 300,
            pad: false,
            pauseLength: 200
        });
    });
</script>

</body>
</html>