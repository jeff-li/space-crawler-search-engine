<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{ title }}</title>
    <meta name="Description" content="Result Page">
    <meta name="Jeff Weizhong Li (998083112)" content="">

    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" />

    <!-- styles -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">
    <link href="/css/pagination_styles.css" rel="stylesheet">
</head>

<body>
    <div id="loading">
        <img id="loading-icon" src="http://bit.ly/pMtW1K">
    </div>

    %if validated == False:
    <div class="login-panel" id="login-panel">
        <form id="loginForm" action="/login" method="GET">
            <button class="login-button" type="submit">
                <img class="login-img" id="person" />
                <span class="login-text"> SIGN IN </span>
            </button>
        </form>
    </div>

    %else:
    <div class="logout-panel">
          <form id="logoutForm" action="/logout" method="GET" onsubmit="">
                <div class="row">
                    <div class="col-sm-3 col-sm-offset-1 profile-sm-frame">
                        <img class="profile-img" src="{{profile['picture']}}" id="person" />
                    </div>
                    <div class="col-sm-7 user-info-area"
                        <span> {{profile['name']}} </span> <br>
                        <span> {{profile['email']}} </span>
                        <button id="logout-button" class="logout-button" type="submit">
                            <span class="logout-text"> LOG OUT </span>
                        </button>
                    </div>
                </div>
           </form>
    </div>
    %end
    <div class="logo-box title-box">
        <h1>  Space  <span class="text-top blink red-text"> : </span>  Crawler</h1>
        <h3>an <span class="rainbow-color"> intelligent </span> web crawler</h3>
        <a href="/"></a>
    </div>
    <div class="overlay">
        <!--<button onclick="javascript:history.back()"> < Back </button>-->
        <div class="row rslt-search-row">
            <div class="col-md-8 col-md-offset-2">
                <form action="/" method="GET">
                    <div class="input-group" id="words-prefetch">
                        <input id="keywords" name="keywords" type="text" class="form-control search-input typeahead" value="{{keywords}}" autofocus="">
                    <span class="input-group-btn">
                         <button class="btn btn-default search-btn" type="submit"> GO </button>
                    </span>
                    </div>
                </form>
            </div>
        </div>

        <div class="row result-row">
            <div class="col-md-8 col-md-offset-2">
                <div class="result-box">
                    <div class="panel panel-default">
                        <div class="panel-heading">Search Result</div>
                            <ul class="search-result">
                                %for url in search_result:
                                %   page = url.strip('/')
                                %   page = page.split('/')
                                %   siteName = ''
                                %   if len(page) > 2:
                                %       siteName = page[2]
                                %   end
                                %   pageName = ""
                                %
                                %   if len(page) > 3:
                                %       pageName = page[-1].split('.')[0].title() + ' - '
                                %   end
                                    <li class="resultLink" id="{{url}}">
                                        <h4> {{pageName}} {{siteName}} </h4>
                                        <a target="_blank" href="{{url}}"> {{url}} </a>
                                    </li>
                                %end
                            </ul>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- Load javascript files -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="/js/jquery-1.11.1.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.quick.pagination.min.js"></script>
<script src="/js/rainbow.js"></script>
<script src="/js/typeahead.bundle.min.js"></script>

<script type="text/javascript">
    $(function() {
        $("ul.search-result").quickPagination({pageSize:"5"});
        $(".title-box").click(function(){
            window.location = $(this).find("a:first").attr("href");
            return false;
        });

        $('.resultLink').click(function () {
            var url = this.id;
            window.open(url, '_blank');
        });

        $(".title-box").click(function(){
            window.location = $(this).find("a:first").attr("href");
            return false;
        });

        $( "#logoutForm" ).submit(function( event ) {
            console.log('here');
            var backlen = history.length;
            history.go(-backlen); // Return at the beginning
            window.location.replace("http://localhost:8080");
        });

        $( "#loginForm" ).submit(function( event ) {
            var form = this;
            $("#loading").css("display", "block");
            event.preventDefault();

            setTimeout( function () {
                form.submit();
            }, 300);
        });

        $( "#searchForm" ).submit(function( event ) {
            var form = this;
            $("#loading").css("display", "block");
            event.preventDefault();

            setTimeout( function () {
                form.submit();
            }, 300);
        });

        $("#login-panel").hover(function(){
            $(this).filter(':not(:animated)').animate({ width: "175px" });
        }, function() {
            $(this).animate({ width: "104px" });
        });

        // $('.blink').blink();


        $('.rainbow-color').rainbow({
            colors: [
                '#FF0000',
                '#f26522',
                '#fff200',
                '#00a651',
                //'#28abe2',
                //'#2e3192',
                '#6868ff'
            ],
            animate: true,
            animateInterval: 200,
            pad: false,
            pauseLength: 200
        });

        var words = new Bloodhound({
          datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          limit: 10,
          prefetch: {
            url: '../data/words_data.json'
          }
        });

        words.initialize();

        $('#words-prefetch .typeahead').typeahead({
            highlight: true
        }, {
          name: 'words',
          displayKey: 'name',
          source: words.ttAdapter()
        });

    });
</script>



</body>
</html>