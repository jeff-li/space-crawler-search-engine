+-------------------------------------------------------+
|                                                       |
|               CSC326 Programming Languages            |
|                     Machine Project                   |
|                                                       |
|                     Space : Crawler                   |
|                                                       |
|*******************************************************|
|                                                       |
|     GROUP NUMBER: 9                                   |
|                                                       |
|     Jeff Weizhong Li  998083112   UTORid:liweizho     |
|     Ahsan Sardar      998422158   UTORid:sardarmo     |
|                                                       |
+-------------------------------------------------------+



Note: this application supports one-word search



****** Setup ******

To deploy the project to an AWS instance:

- run: python deployment.py

To terminate an existing AWS instance

- run: python terminate.py 

* AWS ACCESS_KEY_ID and SECRET_ACCESS_KEY can be modified in the aws_credentials.py file



*********** Bonus Features ***********

- Frontend

    1- Responsive Design for mobile phone and tablet using Bootstrap 3

    2- jQuery Animation for better user experience

    3- Auto-completion in search bar (both homepage and result page)

    4- OAuth2.0 login with Google (disabled when using dynamically generated IP addresses)

    5- Search result with pagination

    6- Display search result with page name and link

    7- Used HTML5 CSS3 for better user experience - each search result will be hightlighted when mouse-over

    8- Optimized workflow - users can do the next search directly on the result page


- Backend - DynamoDB

    This application contains implementation for storing/retreiving
    lexicon, documents, inverted index, and page ranks through DynamoDB.


- Deployment/Termination Script
    
    Interactive user interface



