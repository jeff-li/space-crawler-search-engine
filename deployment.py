import boto.ec2
import os
from subprocess import call
import time
import sys


print ""
print ""
print ""
print " _______  _______  _______  _______  _______  ___   _______  ______    _______  _     _  ___      _______  ______   "
print "|       ||       ||   _   ||       ||       ||   | |       ||    _ |  |   _   || | _ | ||   |    |       ||    _ |  "
print "|  _____||    _  ||  |_|  ||       ||    ___||___| |       ||   | ||  |  |_|  || || || ||   |    |    ___||   | ||  "
print "| |_____ |   |_| ||       ||       ||   |___  ___  |       ||   |_||_ |       ||       ||   |    |   |___ |   |_||_ "
print "|_____  ||    ___||       ||      _||    ___||   | |      _||    __  ||       ||       ||   |___ |    ___||    __  |"
print " _____| ||   |    |   _   ||     |_ |   |___ |___| |     |_ |   |  | ||   _   ||   _   ||       ||   |___ |   |  | |"
print "|_______||___|    |__| |__||_______||_______|      |_______||___|  |_||__| |__||__| |__||_______||_______||___|  |_|"
print ""
print "By:  GROUP NUMBER: 9"
print "    Jeff Weizhong Li  998083112   UTORid:liweizho"
print "    Ahsan Sardar      998422158   UTORid:sardarmo"

print ""
print "==============================================================\n"
print "==                  Creating New AWS Instance               ==\n"
print "==============================================================\n"
print ""

REGION = 'us-east-1'

KEY_NAME = 'csc326-group9-key'
GROUP_NAME = 'csc326-group9'
AMI_NAME = 'ami-88aa1ce0'
INSTANCE_NAME = 't1.micro'

CREDENTIALS_FILE = 'aws_credentials.conf'


def launch_instance():
    if not os.path.isfile("BackEnd/csc326-group9-key.pem"):
        print ">>>>>> ERROR: Missing important file csc326-group9-key.pem"
        print ">>>>>> Please check your source files and try again"

    if oct(os.stat("BackEnd/csc326-group9-key.pem").st_mode & 0777) != '0400':
        print ">>>>>> ERROR!  "
        print ">>>>>> Please set the file permission for csc326-group9-key.pem to 400"
        print ">>>>>> eg. sudo chmod 400 csc326-group9-key.pem"
        return

    # Load aws credentials.
    ACCESS_KEY_ID = ''
    SECRET_ACCESS_KEY = ''
    file = open(CREDENTIALS_FILE);
    for line in file:
        line = line.strip()
        if line.startswith('#') or line == '':
            continue
        tokens = line.split(':');
        if ACCESS_KEY_ID == '':
            ACCESS_KEY_ID = tokens[1]
        elif SECRET_ACCESS_KEY == '':
            SECRET_ACCESS_KEY = tokens[1]
            break

    # EC2 connection to AWS.
    conn = boto.ec2.connect_to_region(REGION,
                                      aws_access_key_id=ACCESS_KEY_ID,
                                      aws_secret_access_key=SECRET_ACCESS_KEY)

    try:
        key = conn.get_all_key_pairs(keynames=[KEY_NAME])[0]
    except conn.ResponseError, e:
        if e.code == 'InvalidKeyPair.NotFound':
            print 'Creating keypair: %s' % KEY_NAME
            key = conn.create_key_pair(KEY_NAME)

            # Create key/pair storage dir. if needed.
            KEY_DIR = os.getcwd()
            key_dir = KEY_DIR
            # key_dir = os.expandusr(key_dir)
            #key_dir = os.expandvars(key_dir)
            #if not os.path.isdir(key_dir) :
            #os.mkdir(key_dir, 0700)
            #print 'Created directory: %s' % key_dir

            # Store key/pair .pem file in dir.
            key.save(KEY_DIR)
            print 'Saved key/pair in directory: %s' % KEY_DIR

    try:
        group = conn.get_all_security_groups(groupnames=[GROUP_NAME])[0]
    except conn.ResponseError, e:
        if e.code == 'InvalidGroup.NotFound':
            print 'Creating Security Group: %s' % GROUP_NAME
            group = conn.create_security_group(GROUP_NAME,
                                               'Security group for ec2 web search engine.')
    try:
        # Enable ability to ping the server.
        group.authorize('icmp', -1, -1, '0.0.0.0/0')
        # Enable SSH.
        group.authorize('tcp', 22, 22, '0.0.0.0/0')
        # Enable HTTP.
        group.authorize('tcp', 80, 80, '0.0.0.0/0')
    except conn.ResponseError, e:
        if e.code == 'InvalidPermission.Duplicate':
            print 'Security group: %s already authorized' % GROUP_NAME


    # Start EC2 instance.
    print "Creating new EC2 instance..."
    reservation = conn.run_instances(AMI_NAME,
                                     key_name=KEY_NAME,
                                     security_groups=[GROUP_NAME],
                                     instance_type=INSTANCE_NAME,
                                     user_data=None)

    # Confirm EC2 instance to runs.
    instance = reservation.instances[0]
    while instance.state != 'running':
        time.sleep(6)
        instance.update()
    print "Instance created!"

    print ""
    print "Waiting for the AWS instance to be stable..."
    print "(Prevent refuse connection error)"
    print "--- If you do get a connection refused error, it might be caused by"
    print "--- Internet connection issues. Please try to run the script again."
    print ""

    for i in range(51):
        sys.stdout.write('\r')
        # the exact output you're looking for:
        sys.stdout.write("[%-50s] %d%%" % ('=' * i, 2 * i))
        sys.stdout.flush()
        time.sleep(1)
    print ""
    print ""

    ip = str(instance.ip_address)

    temp = ip.split('.')
    DOMAIN = 'ubuntu@ec2-' + "-".join(temp) + '.compute-1.amazonaws.com'
    HOST = "ubuntu@" + '.'.join(temp)

    instance.update()
    print instance.state
    while instance.state != 'running':
        time.sleep(5)
        instance.update()
    print instance.state

    print ""
    print "==============================================================\n"
    print "==           Copying Source Files To Remote Server          ==\n"
    print "==============================================================\n"
    print ""
    print ""
    call(['scp', '-o', 'StrictHostKeyChecking=no', "-i", "BackEnd/csc326-group9-key.pem", "-r", '.', DOMAIN + ':~/'])
    print "-- SUCCESS"

    COMMAND = "cd BackEnd; bash server_setup.sh"
    call(['ssh', '-o', 'StrictHostKeyChecking=no', "-i", "BackEnd/csc326-group9-key.pem", HOST, COMMAND])

    print ""
    print 'Server IP address: %s ' % instance.ip_address
    print "HOST: " + HOST
    print "DOMAIN " + DOMAIN
    print ""
    print ""
    print "==>  You can now visit Space:Crawler at:"
    print '==>  ec2-' + "-".join(temp) + '.compute-1.amazonaws.com'
    print ""

    return instance

# Launch web search engine instance.
launch_instance()


