import boto.ec2
import os
import time


print ""
print ""
print ""
print " _______  _______  _______  _______  _______  ___   _______  ______    _______  _     _  ___      _______  ______   "
print "|       ||       ||   _   ||       ||       ||   | |       ||    _ |  |   _   || | _ | ||   |    |       ||    _ |  "
print "|  _____||    _  ||  |_|  ||       ||    ___||___| |       ||   | ||  |  |_|  || || || ||   |    |    ___||   | ||  "
print "| |_____ |   |_| ||       ||       ||   |___  ___  |       ||   |_||_ |       ||       ||   |    |   |___ |   |_||_ "
print "|_____  ||    ___||       ||      _||    ___||   | |      _||    __  ||       ||       ||   |___ |    ___||    __  |"
print " _____| ||   |    |   _   ||     |_ |   |___ |___| |     |_ |   |  | ||   _   ||   _   ||       ||   |___ |   |  | |"
print "|_______||___|    |__| |__||_______||_______|      |_______||___|  |_||__| |__||__| |__||_______||_______||___|  |_|"
print ""
print "By:  GROUP NUMBER: 9"
print "    Jeff Weizhong Li  "
print "    Ahsan Sardar      "

print ""
print "==============================================================\n"
print "==                    Stopping AWS Instance                 ==\n"
print "==============================================================\n"
print ""

REGION = 'us-east-1'

KEY_NAME = 'csc326-group9-key'
GROUP_NAME = 'csc326-group9'
AMI_NAME = 'ami-88aa1ce0'
INSTANCE_NAME = 't1.micro'

CREDENTIALS_FILE = 'aws_credentials.conf'

def stop_instance() :
    if not os.path.isfile("BackEnd/csc326-group9-key.pem"):
        print "Missing important file csc326-group9-key.pem"
        print "Please check your source files and try again"

    if oct(os.stat("BackEnd/csc326-group9-key.pem").st_mode & 0777) != '0400':
        print "ERROR!  "
        print "Please set the file permission for csc326-group9-key.pem to 400"
        print "eg. sudo chmod 400 csc326-group9-key.pem"
        return

    # Load aws credentials.
    ACCESS_KEY_ID = ''
    SECRET_ACCESS_KEY = ''
    file = open(CREDENTIALS_FILE);
    for line in file :
        line = line.strip()
        # Skip comments and empty lines
        if line.startswith('#') or line == '' :
                continue
        tokens = line.split(':');
        if ACCESS_KEY_ID == '' :
                ACCESS_KEY_ID = tokens[1]
        elif SECRET_ACCESS_KEY == '' :
                SECRET_ACCESS_KEY = tokens[1]
                break

    # EC2 connection to AWS.
    conn = boto.ec2.connect_to_region(REGION,
        aws_access_key_id = ACCESS_KEY_ID,
        aws_secret_access_key = SECRET_ACCESS_KEY)

    try :
        key = conn.get_all_key_pairs(keynames = [KEY_NAME])[0]
    except conn.ResponseError, e :
        if e.code == 'InvalidKeyPair.NotFound':
            print 'Creating keypair: %s' % KEY_NAME
            key = conn.create_key_pair(KEY_NAME) 
            KEY_DIR = os.getcwd()
            key_dir = KEY_DIR
            key.save(KEY_DIR)
            print 'Saved key/pair in directory: %s' % KEY_DIR
    
    try :
        group = conn.get_all_security_groups(groupnames = [GROUP_NAME])[0]
    except conn.ResponseError, e :
        if e.code == 'InvalidGroup.NotFound':
            print 'Creating Security Group: %s' % GROUP_NAME
            group = conn.create_security_group(GROUP_NAME,
                'Security group for ec2 web search engine.')
    try :
        # Enable ability to ping the server.
        group.authorize('icmp', -1, -1, '0.0.0.0/0')
        # Enable SSH.
        group.authorize('tcp', 22, 22, '0.0.0.0/0')
        # Enable HTTP.
        group.authorize('tcp', 80, 80, '0.0.0.0/0')
    except conn.ResponseError, e :
         if e.code == 'InvalidPermission.Duplicate' :
              print 'Security group: %s already authorized' % GROUP_NAME

    print ""
    print ""
    print "Please enter the ip address you want to terminated:"
    print "e.g 54.85.219.39"
    user_input = raw_input(">> ")
    user_input = user_input.strip()

    reservations = conn.get_all_reservations()

    instances = conn.get_all_instances()

    # print "All instances: "
    # print instances
    #
    # print "All Reservations"
    # print reservations

    # r = instances[0]
    for reservation in instances:
        for instance in reservation.instances:
            # print instance
            # print instance.ip_address
            if str(instance.ip_address) == user_input:
                print "Found instance with IP address " + user_input
                if (instance.state != 'stopped') and (instance.state != 'terminated'):
                    print "Trying to stop the instance..."
                    instance.stop()
                    while instance.state != 'stopped':
                        time.sleep(5)
                        instance.update()
                    print "Instance " + user_input + " has been stopped!"
                    print ""
                    print "Trying to terminate the instance..."
                    instance.terminate()
                    while instance.state != 'terminated':
                        time.sleep(2)
                        instance.update()
                    print "Instance " + user_input + " has been terminated!"
                    return
                elif instance.state != 'terminated':
                    print ""
                    print "Trying to terminate the instance..."
                    instance.terminate()
                    while instance.state != 'terminated':
                        time.sleep(2)
                        instance.update()
                    print "Instance with IP address: " + user_input + "has already been terminated"
                    return
                else:
                    print "Instance with IP address: " + user_input + "has already been terminated"
                    return
    print "Did not find Instance with IP address: " + user_input
            


# Launch web search engine instance.
stop_instance()


